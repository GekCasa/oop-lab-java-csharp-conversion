﻿using System;
using System.Linq;

namespace Unibo.Oop.Utils
{
    internal class TupleImpl : ITuple
    {
        private Object[] items;

        public TupleImpl(object[] args)
        {
            items = args;
        }

        public object this[int i] => items[i];

        public int Length => items.Length;

        public object[] ToArray()
        {
            return items.ToArray();
        }

        public override String ToString()
        {
            return String.Join(", ", items.ToString());
        }

        public override int GetHashCode()
        {
            return items.Length * 871 / (items.Length % 4);
        }

        public override Boolean Equals (Object obj)
        {
            if (obj != null && obj is TupleImpl)
            {
                return true;
            }
            else
                return false;
        }
    }
}

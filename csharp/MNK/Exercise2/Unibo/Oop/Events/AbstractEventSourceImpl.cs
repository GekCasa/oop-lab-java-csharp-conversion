﻿using System;
using System.Collections.Generic;

namespace Unibo.Oop.Events
{
    internal abstract class AbstractEventSourceImpl<TArg> : IEventEmitter<TArg>, IEventSource<TArg>
    {

        protected abstract ICollection<EventListener<TArg>> Listeners { get;}

        public IEventSource<TArg> EventSource => this;

        public void Bind(EventListener<TArg> eventListener)
        {
            Listeners.Add(eventListener);
        }

        public void Emit(TArg data)
        {
            foreach (var listener in Listeners) 
            {
                listener(data);
            }
        }

        public void Unbind(EventListener<TArg> eventListener)
        {
            Listeners.Remove(eventListener);
        }

        public void UnbindAll()
        {
            Listeners.Clear();
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Unibo.Oop.Utils
{
    public class ArrayMatrix<TElem> : IMatrix<TElem>
    {
        private readonly int rows, columns;
        private readonly List<TElem> elements;

        public ArrayMatrix(int rows, int columns)
            : this(rows, columns, Enumerable.Empty<TElem>())
        {

        }

        public ArrayMatrix(int rows, int columns, IEnumerable<TElem> elements)
        {
            if (rows <= 0 || columns <= 0)
            {
                throw new IndexOutOfRangeException();
            }

            this.rows = rows;
            this.columns = columns;
            this.elements = new List<TElem>(rows * columns);
            this.elements.AddRange(elements);

            while (this.elements.Count < rows * columns)
            {
                this.elements.Add(default(TElem)); // Notice me, ask about me
            }
        }


        public IEnumerator<TElem> GetEnumerator()
        {
            return elements.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int ColumnsCount
        {
            get { return columns; }
        }

        public int RowsCount
        {
            get { return rows; }
        }

        public int Count
        {
            get { return rows * columns; }
        }

        public int CoordDiagonal(int i, int j)
        {
            int d = j - i;
            if (d >= ColumnsCount || d <= -RowsCount)
            {
                throw new IndexOutOfRangeException();
            }
            return d;
        }

        public int CoordAntidiagonal(int i, int j)
        {
            throw new NotImplementedException();
        }

        public int CoordRow(int d, int a)
        {
            throw new NotImplementedException();
        }

        public int CoordColumn(int d, int a)
        {
            throw new NotImplementedException();
        }

        public TElem this[int i, int j]
        {
            get
            {
                EnsureInside(i, j);
                return elements[ToLinearIndex(i, j)];
            }
            set
            {
                EnsureInside(i, j);
                elements[ToLinearIndex(i, j)] = value;
            }
        }

        public void SetDiagonals(int d, int a, TElem value)
        {
            int i = CoordRow(d, a);
            int j = CoordColumn(d, a);
            this[i, j] = value;
        }

        public void SetAll(TElem element)
        {
            for (int i = 0; i < RowsCount; i++)
            {
                for (int j = 0; j < ColumnsCount; j++)
                {
                    this[i, j] = element;
                }
            }
        }

        public void SetAll(Func<int, int, TElem, TElem> setter)
        {
            throw new NotImplementedException();
        }

        public TElem GetDiagonals(int d, int a)
        {
            int i = CoordRow(d, a);
            int j = CoordColumn(d, a);
            return this[i, j];
        }

        public IEnumerable<TElem> GetRow(int i)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TElem> GetColumn(int j)
        {
            EnsureInside(0, j);
            return Enumerable
                .Range(0, rows)
                .Select(i => this[i, j]);
        }

        public IEnumerable<TElem> GetDiagonal(int d)
        {
            if (d >= ColumnsCount || d <= -RowsCount)
            {
                throw new IndexOutOfRangeException();
            }
            if (d >= 0)
            {
                return Enumerable
                    .Range(0, Math.Min(rows, columns))
                    .Where(i => IsInside(i, i + d))
                    .Select(i => this[i, i + d]);
            }
            else
            {
                return Enumerable
                    .Range(0, Math.Min(rows, columns))
                    .Where(i => IsInside(i - d, i))
                    .Select(i => this[i - d, i]);
            }
        }

        public IEnumerable<TElem> GetAntidiagonal(int a)
        {
            throw new NotImplementedException();
        }

        public void ForEachIndexed(Action<int, int, TElem> consumer)
        {
            throw new NotImplementedException();
        }

        public TElem[] ToArray()
        {
            return elements.ToArray();
        }

        public IList<TElem> ToList()
        {
            return new List<TElem>(this.elements);
        }

        protected int ToLinearIndex(int i, int j)
        {
            return i * columns + j;
        }

        protected int ToColumnIndex(int k)
        {
            return k % columns;
        }

        protected int ToRowIndex(int k)
        {
            return k / columns;
        }

        protected bool IsInside(int i, int j)
        {
            return i >= 0 && i < rows && j >= 0 && j < columns;
        }

        protected void EnsureInside(int i, int j)
        {
            if (!IsInside(i, j))
            {
                throw new IndexOutOfRangeException();
            }
        }
    }
}
package it.unibo.oop.mnk.ui.console.view;

import it.unibo.oop.mnk.MNKMatch;
import it.unibo.oop.mnk.Symbols;
import it.unibo.oop.util.ImmutableMatrix;

import java.util.Optional;

public interface MNKConsoleView {
    void renderNextTurn(MNKMatch model, int turn, Symbols player, ImmutableMatrix<Symbols> state);
    void renderEnd(MNKMatch model, int turn, Optional<Symbols> winner, ImmutableMatrix<Symbols> state);
    MNKMatch getModel();
    void setModel(MNKMatch match);

    static MNKConsoleView of() {
        return new MNKConsoleConsoleViewImpl();
    }

    static MNKConsoleView of(MNKMatch model) {
        final MNKConsoleView view = new MNKConsoleConsoleViewImpl();
        view.setModel(model);
        return view;
    }
}

package it.unibo.oop.util;

public interface Tuple3<A, B, C> extends Tuple {
	A getFirst();
    B getSecond();
    C getThird();
}

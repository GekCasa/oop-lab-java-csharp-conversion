package a02a.e1;

import a02a.e1.SequenceAcceptor.Sequence;

public class SequenceAcceptorImpl implements SequenceAcceptor {

	private SequenceAcceptor.Sequence currentSeq;
	private int next;
	private int cur=0;
	private int help=0;
	
	@Override
	public void reset(SequenceAcceptor.Sequence sequence) {
		currentSeq = sequence;
	}

	@Override
	public void reset() {
	}

	@Override
	public void acceptElement(int i) {
		next = nextExpected();
		if(i != next) {
			throw new IllegalStateException();
		}

	}
	
	private int nextExpected() {
		if (currentSeq == Sequence.POWER2) {
			nextPower();
		}
		else if (currentSeq == Sequence.FLIP) {
			nextFlip();
		}
		else if (currentSeq == Sequence.RAMBLE) {
			 nextRamble();
		}
		else {
			nextFibo();
		}
		
		return cur;
	}
	
	private void nextPower() {
		if(cur!=0) {
			cur = (cur==1) ? 2 : cur*2;
		}
		else {
			cur=1;
		}
		
	}
	
	private void nextFlip() {
		cur = (cur==0) ? 1 : 0;
	}

	private void nextRamble() {
		if(help==0) {
			help=cur+1;
			cur=0;
		}
		else {
			cur=help;
			help=0;
		}
	}
	
	private void nextFibo() {
	}

}

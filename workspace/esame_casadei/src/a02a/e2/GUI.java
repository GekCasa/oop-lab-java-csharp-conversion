package a02a.e2;

import java.util.ArrayList;
import java.util.Collection.*;
import java.util.List;
import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
	final List<JButton> buttons = new ArrayList<>();
    
	public GUI(int size) {
    	final ModelTris model = new ModelTris(size);
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        int cols = size; // {1,2,3,4,5}
        JPanel panel = new JPanel(new GridLayout(cols,cols));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        for (int i=0;i<size*size;i++){
           final JButton jb = new JButton(" ");
           final int index=i;
           buttons.add(jb);
            jb.addActionListener(e -> {
            	jb.setText(model.change(index));
            	if(model.check()) {
                	System.exit(1);
                }
            });
            panel.add(jb);
        } 
        this.setVisible(true);
    }
    
    
}

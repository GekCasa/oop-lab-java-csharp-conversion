package a02a.e2;

import java.util.ArrayList;
import java.util.Collection.*;
import java.util.List;

public class ModelTris implements Tris {

	private final List<Boolean> game;
	private final int size;
	
	public ModelTris(int size) {
		this.size=size;
		game = new ArrayList<>();
		for(int i=0; i<size*size; i++) {
			game.add(false);
		}
	}

	@Override
	public String change(int i) {

		game.set(i, (!game.get(i)) ? true : false);
		return !game.get(i) ? " " : "X";

	}

	@Override
	public boolean check() {
		return checkColumns() || checkRows();

	}
	
	private boolean checkColumns() {
		
		int equal = 0;
		for(int i=0; i<size; i++) {
			for(int j=0; j<size*size; j+=size) {				
				if(game.get(i+j)) {
					equal++;
				}
			}
			if(equal==size) {
				return true;
			}
			equal = 0;
			
		}
		return false;
	}
	
	private boolean checkRows() {
		int equal = 0;
		for(int j=0; j<size*size; j+=size) {					
			for(int i=0; i<size; i++) {
				if(game.get(i+j)) {
					equal++;
				}
			}
			if(equal==size) {
				return true;
			}
			equal = 0;
			
		}
		return false;
	}

}
